<page xmlns="http://projectmallard.org/1.0/"
      type="guide" style="task"
      id="admin-entites" xml:lang="fr">

<info>
  <link type="guide" xref="admin-valideur" group="entites"/>
  <link type="guide" xref="admin-admin" group="entites"/>
  <desc>Composantes et services de l'université.</desc>
</info>

<title>Gestion des entités</title>

<p>Les composantes et les services intégrés à l'application proviennent de l'annuaire de l'université. C'est un ensemble complexe.</p>

<p>Il est possible d'effectuer des recherches dans la liste des entités pour identifier le service ou la composante recherchée. Pour cela il faut cliquer sur « Entités administratives ». </p>

<figure>
  <title>Liste des entités</title>
  <media type="image" mime="image/png" src="figures/liste-entites.png" >
  </media>
</figure>

<p>Les entités sont organisées sous forme d'aborescence et deux outils sont disponibles pour effectuer des recherches dans cette arborescence :</p>
<list>
  <item><p>La zone de texte qui va permettre de rechercher une chaîne de caractère dans toute l'arborescence.</p></item>
  <item><p>La liste déroulantes « Filtres » qui permet de limiter l'affichage à un niveau particulier de l'arborescence</p></item>
</list>

<p>Les deux outils sont articulés par l'opérateur booléen « et ».</p>

<p>Si le nom d'une entité, extraite de l'annuaire, ne convient pas on peut la renommer en cliquant sur son nom, en saisissant le nouveau nom et en cliquant sur enregistrer.</p>

<p>Après sélection d'une entité grâce à la case à cocher, choisir « Supprimer les Entités sélectionnées » dans la liste déroulante en bas à gauche de l'écran permet de la supprimer.</p>

</page>
