<page xmlns="http://projectmallard.org/1.0/"
      type="guide" style="task"
      id="admin-profils-impression" xml:lang="fr">

<info>
  <link type="guide" xref="admin-admin" group="profils-impression"/>
  <desc>Paramétrage des différents profils d'impression et de leurs options (papier, reliure, couleur...)</desc>
</info>

<title>Profils de reprographie</title>

<p>Les choix que l'utilisateur aura à sa disposition concernant le type de reprographie demandé (type de papier, type de reliure, etc.), sont définis dans l'interface d'administration dans la rubrique « Profils de reprographie ».</p>

<section id="profils">
<title>Profils d'impression</title>

<p>En cliquant sur « Profils d'impression » on peut ajouter, modifier ou supprimer un profils.</p>

<figure>
  <title>Liste des profils d'impression</title>
  <media type="image" mime="image/png" src="figures/liste-profils-impression.png" >
  </media>
</figure>

<p>Un clic sur l'ID du profil permet de le modifier.</p>

<figure>
  <title>Édition d'un profil d'impression</title>
  <media type="image" mime="image/png" src="figures/edition-profils-impression.png" >
  </media>
</figure>

<p>Il est alors possible d'ajouter ou de supprimer les choix qui seront associés au profil édité en les faisant passer d'un cadre à l'autre à l'aide des flèches situées entre les deux, puis de valider en cliquant sur le bouton « Enregistrer ». Un utilisateur sélectionnant ce profil aura ainsi automatiquement choisi, pour la reproduction de son document, l'ensemble des caractéristiques se trouvant dans le cadre de droite.</p>

<p>L'édition du profil permet également d'en définir les éléments suivants :</p>
<list>
  <item><p>Nom du profil : le nom tel qu'il sera affiché à l'utilisateur.</p></item>
  <item><p>Prix par page : qui sera multiplié par le nombre de page du document pour déterminer son coût.</p></item>
  <item><p>Prix par document : qui sera ajouté au prix des pages pour obtenir le coût global du document.</p></item>
  <item><p>Description : une description facultative du profil.</p></item>
  <item><p>Ordre : chiffre qui permet de définir à quelle place le profil doit apparaître pour l'utilisateur.</p></item>
</list>

<p><em style="strong">Ajouter un profil</em> : Un clic sur le bouton « Ajouter Profil d'impression » permet d'en créer un nouveau.</p>
<p><em style="strong">Supprimer un profil</em> : Après sélection du profil grâce à la case à cocher, choisir « Supprimer les Profils d'impression sélectionnés » dans la liste déroulante en bas à gauche de l'écran permet de le supprimer.</p>
</section>

<section id="options">
<title>Options des profils d'impression</title>

<p>En cliquant sur « Profils d'impression : Options » on peut ajouter, modifier ou supprimer les options disponibles pour la définition des profils.</p>

<figure>
  <title>Liste des options</title>
  <media type="image" mime="image/png" src="figures/liste-options.png" >
  </media>
</figure>

<p>Un clic sur l'ID de l'option permet de la modifier.</p>

<figure>
  <title>Édition d'une option</title>
  <media type="image" mime="image/png" src="figures/edition-option.png" >
  </media>
</figure>

<p>Il est alors possible de définir les caractéristiques de l'option :</p>

<list>
  <item><p>Nom de l'option : le nom tel qu'il sera affiché à l'utilisateur.</p></item>
  <item><p>Visible : case à cocher permettant d'afficher/masquer l'option à l'utilisateur.</p></item>
  <item><p>Type de choix : permet de déterminer sous quelle forme l'option apparaîtra à l'écran. 4 formes sont disponibles :</p>
    <list>
      <item><p>Radio : un bouton radio</p></item>
      <item><p>Liste : une liste déroulante</p></item>
      <item><p>Cases à cocher : cases à cocher permettant de sélectionner plusieurs choix</p></item>
      <item><p>Liste à choix multiple : une liste permettant de sélectionner plusieurs choix</p></item>
    </list>
  </item>
  <item><p>Description : une description facultative de l'option.</p></item>
  <item><p>Ordre : chiffre qui permet de définir à quelle place l'option doit apparaître pour l'utilisateur.</p></item>
  <item><p>Choix : définit les différents choix disponibles pour cette option. On peut ajouter un choix en cliquant « Ajouter un objet Profils D'Impression : Choix supplémentaire » et les supprimer en cliquant sur le X en face de leur nom. Chaque choix est défini par :</p>
  <list>
    <item><p>Nom : Nom du choix</p></item>
    <item><p>Prix par page : qui sera multiplié par le nombre de page du document pour déterminer son coût.</p></item>
    <item><p>Prix par document : qui sera ajouté au prix des pages pour obtenir le coût global du document.</p></item>
    <item><p>Description : un description facultative de l'option.</p></item>
    <item><p>Ordre : chiffre qui permet de définir à quelle place l'option doit apparaître pour l'utilisateur.</p></item>
  </list>
  </item>
</list>

<p><em style="strong">Ajouter une option</em> : Un clic sur le bouton « Ajouter Profil d'impression : Option » permet d'en créer une nouvelle.</p>
<p><em style="strong">Supprimer une option</em> : Après sélection d'une option grâce à la case à cocher, choisir « Supprimer les Profils d'impression : Options sélectionnés » dans la liste déroulante en bas à gauche de l'écran permet de la supprimer.</p>
</section>

</page>
