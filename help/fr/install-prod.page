<page xmlns="http://projectmallard.org/1.0/"
      type="topic" style="task"
      id="install-prof" xml:lang="fr">

<info>
  <desc>Mise en production.</desc>
  <link type="guide" xref="install"/>
  <title type="sort">3 - prod</title>
</info>

<title>Passage en production</title>

<links type="section" />

<section id="config">
<title>Configuration de l'application</title>
<p>Pour rappel, la configuration de l'application se fait dans
<code>/etc/polynum/local_settings.py</code></p>

<p>Un fichier d'exemple <code>local_settings.py.example</code> est fourni avec
Polynum, c'est un modèle pour la création de
<code>/etc/polynum/local_settings.py</code> : il contient la liste des
paramétrages possibles avec explications et exemples.</p>

<p>Si vous avez installé Polynum à partir du paquet, le modèle est placé
dans <code>/usr/share/doc/polynum/</code>, il vous suffit donc de faire :</p>

<screen>
<output style="prompt"># </output><input>cd /etc/polynum</input>
<output style="prompt"># </output><input>cp /usr/share/doc/polynum/local_settings.py.example local_settings.py</input>
<output style="prompt"># </output><input>vi local_settings.py</input>
</screen>

<note style="warning">
<p>Attention : la configuration <code>/etc/polynum/local_settings.py</code>
doit être lisible par l'utilisateur qui va faire tourner l'application,
c'est-à-dire en général <code>polynum</code>. Par ailleurs ce fichier va
contenir des de passe (accès à la base de données, à l'annuaire LDAP), il ne
doit donc pas être lisible par d'autres utilisateurs :</p>
<screen>
<output style="prompt"># </output><input>chown polynum:polynum /etc/polynum/local_settings.py</input>
<output style="prompt"># </output><input>chmod 640 /etc/polynum/local_settings.py</input>
</screen>
</note>
</section>

<section id="installation-sql">
<title>Installation de la base de données</title>
<p>La base de données attendue par PolyNum est PostgreSQL.</p>
<steps>
<item>
<title>Création de la base et d'un utilisateur associé</title>
<p>Sur le serveur PostgreSQL, créer une base vierge (<code>createdb polynum</code>) et un
utilisateur qui aura tous les droits sur cette base (lecture, écriture, mais aussi
création et mise à jour des tables).</p>
<p>Dans la suite, la base sera nommée <code>polynum</code> et l'utilisateur
<code>polynum</code>.</p>
</item>
<item>
<title>Configuration de l'accès à la base dans PolyNum</title>
<p>Dans <code>/etc/polynum/local_settings.py</code> modifier le paramètre <code>DATABASES</code>
selon le modèle suivant :</p>
<code>
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'polynum',    # nom de la base
        'USER': 'polynum',    # nom de l'utilisateur associé
        'PASSWORD': '...',    # mot de passe de l'utilisateur
        'HOST': 'db.univ.fr', # serveur contenant la base (rien pour localhost)
        'PORT': '',           # port TCP (rien pour le port par défaut)
    }
}
</code>
</item>
<item>
<title>Création des tables de la base</title>
<p>Une fois la configuration enregistrée, lancez la création de la base :</p>
<screen>
<output style="prompt"># </output><input>polynum-manage.py syncdb</input><output>
Installing custom SQL ...
Installing indexes ...
Installed 0 object(s) from 0 fixture(s)

Synced:
 > south
 > django.contrib.auth
(...)
Not synced (use migrations):
 - polynum.base
 - polynum.delegation
(use ./manage.py migrate to migrate these)</output>
</screen>
<screen>
<output style="prompt"># </output><input>polynum-manage.py migrate</input>
Running migrations for base:
(...)
Installed 0 object(s) from 0 fixture(s)
</screen>
</item>
<item>
<title>Création des index pour optimisation</title>
<p>Certaines requêtes nécessitent des index pour être performantes, il est
donc indispensable de créer des index d'optimisation :</p>
<screen>
<output style="prompt"># </output><input>polynum-manage.py sqlindexes base delegation | \
                    psql --host=db.univ.fr --username=polynum </input>
</screen>
</item>
</steps>
</section>

<section id="installation-init">
<title>Mise en place de données initiales</title>
<p>Pour aider à la configuration et l'adaptation de PolyNum au contexte local, des
données initiales «classiques» sont livrées, appelés <em>fixtures</em> dans le monde
Django. Pour les ajouter dans la base :
</p>
<screen>
<output style="prompt"># </output><input>polynum-manage.py loaddata /usr/share/doc/polynum/fixtures/*.json</input>
</screen>
</section>

<section id="installation-entities">
<title>Mise en place des entités</title>
<p>Chaque demande dans PolyNum est liée à une entité administrative. Ces entités
doivent être importées dans PolyNum depuis un dump au format LDIF/SupAnn ou XML (un
exemple de fichier XML est disponible dans <code>/usr/share/doc/polynum/fixtures/structures.xml</code>.</p>
<p>Une fois le fichier LDIF ou XML obtenu, l'importer avec la commande <code>loadentities</code> :</p>
<terms>
<item>
<title>Depuis un fichier XML :</title>
<screen>
<output style="prompt"># </output><input>polynum-manage.py loadentities /.../structures.xml</input>
<output>(...)
Added   7980 entities
</output>
</screen>
</item>
<item>
<title>Depuis un fichier LDIF :</title>
<screen>
<output style="prompt"># </output><input>polynum-manage.py loadentities /.../structures.ldif</input>
<output>(...)
Added   7980 entities
</output>
</screen>
</item>
</terms>
</section>

<section id="installation-auth">
<title>Configuration de l'authentification</title>
<p>Polynum peut authentifier les utilisateurs via CAS et LDAP. Dans
<code>/etc/polynum/local_settings.py</code> adapter les paramètres suivants
:</p>

<code>

(... extrait de /etc/polynum/local_settings.py ...)

CAS_SERVER_URL = "https://cas.univ.fr/" # CAS server URL
LDAP_URL = "ldaps://ldap.univ.fr"       # LDAP server URL
LDAP_BIND_DN = "uid=...,ou=...,dc=..."  # DN to authenticate to the LDAP server
LDAP_BIND_PASSWORD = "..."              # password
LDAP_USER_QUERY = "supannAliasLogin=%s" # query to retrieve user, %s is CAS username
LDAP_BASE = "dc=univ,dc=fr"             # base DN for the query
</code>

</section>

<section id="installation-init.d">
<title>Lancement de l'application au démarrage du système</title>
<p>TODO : sysvinit et /etc/default/polynum</p>
</section>

<section id="installation-debug">
<title>Retrait du mode <em>debug</em>, collectstatic</title>
<p>TODO : DEBUG = False</p>
</section>

<section id="installation-reverseproxy">
<title>Mise en production derrière un reverse-proxy</title>
<p>TODO : collectstatic</p>
<p>TODO : config gunicorn + nginx</p>
</section>

</page>

