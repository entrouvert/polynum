<page xmlns="http://projectmallard.org/1.0/"
      type="guide" style="task"
      id="admin-utilisateurs" xml:lang="fr">

<info>
  <link type="guide" xref="admin-admin" group="groupes"/>
  <desc>Création et modification des utilisateurs dans l'application.</desc>
</info>

<title>Gestion des utilisateurs</title>

<p>Depuis la page d'accueil de l'administration, on peut accéder au listing des utilisateurs en cliquant sur « Utilisateurs ».</p>

<figure>
  <title>Liste des utilisateurs</title>
  <media type="image" mime="image/png" src="figures/liste-utilisateurs.png" >
  </media>
</figure>

<p>Pour ajouter un utilisateur, il faut cliquer sur le bouton « Ajout d'un utilisateur ». À noter que c'est aussi possible depuis la page d'accueil de l'administration, en cliquant sur sur lien « Ajouter » en face de « Utilisateurs ».</p>

<note style="warning">
  <p>Il n'est pas possible de supprimer un utilisateur, ces derniers provenant de l'annuaire de l'université, ils sont supprimés dans cet annuaire lorsqu'ils doivent l'être.</p>
</note>  

<p>Un clic sur le nom d'un utilisateur permet de l'éditer.</p>

<figure>
  <title>Édition utilisateurs</title>
  <media type="image" mime="image/png" src="figures/utilisateur.png" >
  </media>
</figure>

<p>Cela permet de modifier son nom mais surtout de choisir quels sont ses rôles et de les lier à la composante ou au service dont il dépend.</p>

<note>
  <p>Pour chercher rapidement dans la liste des utilisateurs, on peut utiliser la zone texte au-dessus de la liste. Il suffit de saisir tout ou partie de la chaîne de caractère recherchée et de cliquer sur la loupe.</p>
</note>  

</page>
