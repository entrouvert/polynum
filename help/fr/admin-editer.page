<page xmlns="http://projectmallard.org/1.0/"
      type="guide" style="task"
      id="admin-editer" xml:lang="fr">

<info>
    <link type="guide" xref="admin-admin" group="editer"/>
  <desc>Utiliser l'éditeur de texte pour modifier les différents paragraphes éditables de l'application.</desc>
</info>

<title>Utiliser l'éditeur intégré</title>

<p>L'application dispose d'un éditeur intégré qui permet de modifier toutes les pages présentées à un utilisateur qui fait une demande de reprographie. Pour activer l'éditeur il faut cliquer sur le bouton « Activer l'éditeur » dans la barre supérieure.</p>

<p>Les différentes zones éditables apparaîssent alors encadrées en jaune. Un clic sur une zone permet d'en éditer et d'en modifier le contenu. Une barre d'outil apparaît au-dessus de la zone pour permettre de réaliser les opérations classiques d'un traitement de texte : mise en forme, style, liste à puce, liens, insertion d'image, etc.</p>


<figure>
  <title>Barre d'outil de l'éditeur</title>
  <media type="image" mime="image/png" src="figures/barre-outil-editeur.png" >
  </media>
</figure>

<p>Lorsque l'édition est terminée, il suffit d'appuyer sur la touche entrée pour valider les modifications.</p>

<note>
  <title>Déplacer la barre d'outil de l'éditeur</title>
  <p>La barre d'outil peut être déplacée grâce à la la barre de titre visible lors du survol de la souris à côté des onglets. IL suffit de cliquer/déplacer sur cette barre de titre pour pouvoir mettre la barre d'outil à l'endroit souhaité.</p>
</note>

</page>
