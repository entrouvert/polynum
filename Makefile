all:
	@echo "You can do ./make [update-fixtures]"

update-fixtures:
	./manage.py dumpdata auth.group | indent >polynum/base/fixtures/groups.json
	./manage.py dumpdata base.role | indent >polynum/base/fixtures/roles.json
	./manage.py dumpdata base.status | indent >polynum/base/fixtures/status.json
	./manage.py dumpdata base.transition base.mailnotification base.action | indent >polynum/base/fixtures/workflow.json
	./manage.py dumpdata base.profileoptionchoice base.profile base.profileoption base.documentusage base.documentlicence base.deliveryplace | indent >polynum/base/fixtures/profiles.json

create-ldap-tunnel:
	ssh -v -p5022 -fN -L2000:ldap.ent.dauphine.fr:636 sshgate@cerbere.dsi.dauphine.fr

rebuild-indexes:
	python manage.py sqlindexes base | sed 's/CREATE/DROP/;s/INDEX/INDEX IF EXISTS/;s/ ON.*;/;/' | psql polynum
	python manage.py sqlindexes base | psql polynum


# mainly for packages creation
INSTALL=/usr/bin/install
CP=/bin/cp
install:
	$(INSTALL) -d $(DESTDIR)/opt/polynum
	$(CP) -r polynum $(DESTDIR)/opt/polynum
	$(CP) -r bin $(DESTDIR)/opt/polynum
	$(INSTALL) -d $(DESTDIR)/etc/polynum
	$(INSTALL) -d $(DESTDIR)/var/lib/polynum
	$(INSTALL) -d $(DESTDIR)/opt/polynum/static

deb:
	dch --newversion=`git describe --long | tr - .`-1
	dch -r
	cd debian && pdebuild

