# preparation :
#   $ yum install git
#   $ mkdir -p ~/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
#   $ echo '%_topdir %(echo $HOME)/rpmbuild' >> ~/.rpmmacros
#   $ echo | gzip -c > ~/rpmbuild/SOURCES/polynum-0.0.tar.gz   # create a first tar.gz (fake)
#   $ cd ~/dev && git clone git://repos.entrouvert.org/polynum.git
#   $ ln -s ~/dev/polynum/rpm/centos.spec ~/rpmbuild/SPECS/polynum.spec
#   $ rpmbuild -ba ~/rpmbuild/SPECS/polynum.spec


#
# git macros
#
# from http://wiki.mandriva.com/en/Rpmbuild_and_git#First-time_build_for_a_project

%define git_repo polynum
%define git_head master

%define git_repodir %(echo ~/dev/)
%define git_gitdir %{git_repodir}/%{git_repo}/.git

%define git_get_source pushd %{git_repodir}/%{git_repo} ;\
        /usr/bin/git archive --format=tar --prefix=%{name}-%{version}/ %{git_head} | \
                gzip -c > %{_sourcedir}/%{name}.tar.gz ;\
        popd

%define git_prep_submodules pushd %{git_repodir}/%{git_repo} ;\
	git submodule init ;\
	git submodule update ;\
	popd

# use tags like "MAJOR.MINOR"
%define git_version %(git --git-dir=%{git_gitdir} describe --long)
%define git_get_ver %(echo %{git_version} | sed 's/^v\\?\\(.*\\)-\\([0-9]\\+-g.*\\)$/\\1/;s/-//')
%define git_get_rel %(echo %{git_version} | sed 's/^v\\?\\(.*\\)-\\([0-9]\\+-g.*\\)$/\\2/;s/-/_/')

#
# real spec starts here
#

# Header Stanza begins here

Summary: Polycopiés numériques
Name: polynum

Version: %git_get_ver
Release: %git_get_rel
License: AGPLv3+
Group: Applications/Internet
URL: http://dev.entrouvert.org/projects/polynum

Source0: %{name}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch: noarch

Provides: polynum
# depends
Requires: python26
Requires: python26-virtualenv
Requires: python26-ldap
Requires(pre): /usr/sbin/groupadd, /usr/sbin/useradd
Requires(post): chkconfig
Requires(preun): chkconfig
# This is for /sbin/service
Requires(preun): initscripts

%description
Polynum (polycopiés numériques) est une application web pour demander
l'impression de polycopiés via l'ENT. Elle propose des options de diffusion
afin de mettre à disposition en ligne les polycopiés pour les étudiants.

%prep
%git_get_source
%git_prep_submodules
%setup -q

%build

# wtf... we don't want .pyo and .pyc files
%define __os_install_post echo "NO COMPRESS"

%install
%{__rm} -rf %{buildroot}
make DESTDIR=%{buildroot} install
%{__mkdir} -p %{buildroot}/%{_sysconfdir}/%{name}
%{__mkdir} -p %{buildroot}/%{_sysconfdir}/sysconfig
%{__mkdir} -p %{buildroot}/%{_sysconfdir}/init.d
%{__install} -m 644 rpm/sysconfig %{buildroot}/%{_sysconfdir}/sysconfig/%{name}
%{__install} -m 755 rpm/init.d %{buildroot}/%{_sysconfdir}/init.d/%{name}
%{__mkdir} -p %{buildroot}/usr/bin
ln -s /opt/polynum/bin/polynum-manage.py %{buildroot}/usr/bin

%clean

%files
# set perms and ownerships of packaged files
# the - indicates that the current permissions on the files should be used
%defattr(-,root,root)
%doc requirements help local_settings.py.example gunicorn_config.py.example README polynum/base/fixtures
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
/opt/polynum/*
/etc/init.d/*
/usr/bin/*
# empty dirs
%dir %{_sysconfdir}/%{name}
%dir /opt/polynum/static
%dir /var/lib/polynum

%pre
# add polynum group & user
/usr/sbin/groupadd -r polynum 2> /dev/null || :
/usr/sbin/useradd -r -c "PolyNum daemon user" -d /opt/polynum -M -g polynum -s /sbin/nologin polynum 2> /dev/null || :

%post
# This adds the proper /etc/rc*.d links for the script
/sbin/chkconfig --add polynum

[ -f "%{_sysconfdir}/sysconfig/%{name}" ] && . %{_sysconfdir}/sysconfig/%{name}
# Sane defaults:
[ -z "$POLYNUM_USER" ] && POLYNUM_USER=polynum
[ -z "$POLYNUM_NAME" ] && POLYNUM_NAME="PolyNum daemon user"
[ -z "$POLYNUM_HOME" ] && POLYNUM_HOME=/opt/polynum
[ -z "$POLYNUM_GROUP" ] && POLYNUM_GROUP=polynum
[ -z "$POLYNUM_VAR" ] && POLYNUM_VAR=/var/lib/polynum
[ -z "$POLYNUM_ETC" ] && POLYNUM_ETC=%{_sysconfdir}/polynum
[ -z "$PIDFILE" ] && PIDFILE=/var/run/polynum/polynum.pid

# create default config 
# and adjust some files and directories permissions
if [ ! -f $POLYNUM_ETC/local_settings.py ]
then
	echo -n "Installing default $POLYNUM_ETC/local_settings.py .."
	cp /usr/share/doc/polynum-%{version}/local_settings.py.example $POLYNUM_ETC/local_settings.py
	chown $POLYNUM_USER:$POLYNUM_GROUP $POLYNUM_ETC/local_settings.py
	chmod u=rw,g=,o= $POLYNUM_ETC/local_settings.py
	echo "..done"
fi

echo -n "Creating $POLYNUM_VAR/media .."
mkdir -p $POLYNUM_VAR/media 2> /dev/null || true
chown -R $POLYNUM_USER:$POLYNUM_GROUP $POLYNUM_VAR/media
chmod u=rwx,g=rwxs,o= $POLYNUM_VAR/media
echo "..done"

PIDDIR=$(dirname ${PIDFILE})
echo -n "Creating $PIDDIR .."
mkdir -p $PIDDIR 2> /dev/null || true
chown -R $POLYNUM_USER:$POLYNUM_GROUP $PIDDIR
chmod u=rwx,g=rwxs,o= $PIDDIR
echo "..done"

# create pyc with python2.6
echo -n "Compiling py->pyc in "
for pydir in polynum bin virtualenv
do
    echo -n "$POLYNUM_HOME/$pydir "
    python2.6 -m compileall -f -q $POLYNUM_HOME/$pydir
done
echo "...ok"


%preun
# 'if [ $1 -eq 0 ] ' checks that this is the actual deinstallation of the
# package, as opposed to just removing the old package on upgrade. These
# statements stop the service, and remove the /etc/rc*.d links.
if [ $1 -eq 0 ] ; then
    /sbin/service polynum stop # >/dev/null 2>&1
    /sbin/chkconfig --del polynum
fi

%changelog
* Tue Jul 24 2012 Thomas NOEL <tnoel@entrouvert.com> 0.0-1
- create spec file

