#!/bin/sh
#
# polynum Polycopies Numeriques
#
# chkconfig:   2345 20 80
# description: PolyNum web application

### BEGIN INIT INFO
# Provides: 
# Required-Start: $local_fs $network $syslog
# Required-Stop: 
# Should-Start: 
# Should-Stop: 
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: PolyNum
# Description: Polycopies Numeriques
### END INIT INFO

# Source function library.
. /etc/rc.d/init.d/functions

exec="/opt/polynum/bin/polynum-manage.py"
prog="polynum"
config="/etc/polynum/local_settings.py"

# polynum config
POLYNUM_USER=polynum
POLYNUM_GROUP=polynum
PIDFILE=/var/run/polynum/polynum.pid
[ -e /etc/sysconfig/$prog ] && . /etc/sysconfig/$prog

lockfile=/var/lock/subsys/$prog

start() {
    [ -x $exec ] || exit 5
    [ -f $config ] || exit 6
    echo -n $"Starting $prog: "
    [ x$START_DAEMON != xyes ] && echo " disable by /etc/sysconfig/$prog" && exit 6
    daemon --user $POLYNUM_USER --pidfile $PIDFILE $exec $DAEMON_ARGS
    retval=$?
    echo
    [ $retval -eq 0 ] && touch $lockfile
    return $retval
}

stop() {
    echo -n $"Stopping $prog: "
    killproc -p $PIDFILE $prog
    retval=$?
    echo
    [ $retval -eq 0 ] && rm -f $lockfile
    return $retval
}

restart() {
    stop
    start
}

reload() {
    restart
}

force_reload() {
    restart
}

rh_status() {
    # run checks to determine if the service is running or use generic status
    status -p $PIDFILE $prog
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}


case "$1" in
    start)
        rh_status_q && exit 0
        $1
        ;;
    stop)
        rh_status_q || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    reload)
        rh_status_q || exit 7
        $1
        ;;
    force-reload)
        force_reload
        ;;
    status)
        rh_status
        ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart|reload|force-reload}"
        exit 2
esac
exit $?

