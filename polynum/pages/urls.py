from django.conf.urls import url, patterns
import views

urlpatterns = patterns('',
    url(r'^/*$', views.homepage, name='homepage'),
    url(r'^about/*$', views.about, name='about'),
    url(r'^contact/*$', views.contact, name='contact'),

    #
    url(r'^dashboard/*$', views.dashboard, name='dashboard'),
)

