import hashlib

from django.conf import settings


EDITOR_CACHE_TIMEOUT = getattr(settings, 'EDITOR_CACHE_TIMEOUT', 3600*8)

def cache_key(name):
    return 'editor:' + hashlib.md5(name).hexdigest()
