from django.conf.urls import url, patterns

import views

urlpatterns = patterns('',
    url(r'^$', views.handler, name='editor-handler'),
)
