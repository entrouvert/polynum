from django.contrib import admin

class ContentBlockAdmin(admin.ModelAdmin):
    list_display = ('name', 'content')
