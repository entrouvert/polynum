# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'ContentBlock.name'
        db.alter_column('editor_contentblock', 'name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=512))
    def backwards(self, orm):

        # Changing field 'ContentBlock.name'
        db.alter_column('editor_contentblock', 'name', self.gf('django.db.models.fields.CharField')(max_length=64, unique=True))
    models = {
        'editor.contentblock': {
            'Meta': {'object_name': 'ContentBlock'},
            'content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '512'})
        }
    }

    complete_apps = ['editor']