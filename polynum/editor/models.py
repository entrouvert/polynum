from django.db import models
from django.utils.translation import ugettext_lazy as _

class ContentBlock(models.Model):
    class Meta:
        verbose_name = _('Contenu')
        permissions = (("edit_site", u"Can edit the site"),)

    name = models.CharField(verbose_name=_('Name'), max_length=512, unique=True)
    content = models.TextField(verbose_name=_('Content'), blank=True)

# Create your models here.
