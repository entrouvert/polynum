from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url(r'entity/$', views.entity),
    url(r'entity/(?P<code>\d+)/$', views.entity),
    url(r'entity/(?P<code>\d+)/parents/$', views.entity_parents),
)
