# -*- encoding: utf-8 -*-

"""
django-admin-tools : this file was generated with the customdashboard
management command, it contains the two classes for the main dashboard and app
index dashboard.  You can customize these classes as you want.

See http://django-admin-tools.readthedocs.org/en/latest/dashboard.html
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for PolyNum
    """

    columns = 2
    title = None

    def init_with_context(self, context):

        site_name = get_admin_site_name(context)

        self.children.append(modules.ModelList(
            _(u'Groupes, utilisateurs et rôles'),
            layout='inline',
            draggable=True,
            collapsible=True,
            deletable=False,
            models=('polynum.base.models.user.PolynumUser',
                'django.contrib.auth.models.User',
                'django.contrib.auth.models.Group',
                'polynum.base.models.rbac.Role*',
                'polynum.delegation.models.Delegation'),
        ))

        self.children.append(modules.ModelList(
            _(u'Demandes'),
            draggable=True,
            collapsible=True,
            deletable=False,
            models=('polynum.base.models.request.Request',
                ),
        ))

        self.children.append(modules.ModelList(
            _(u'Paramètres des documents'),
            draggable=True,
            collapsible=True,
            deletable=False,
            models=(
                 'polynum.base.models.request.DeliveryPlace',
                 'polynum.base.models.request.DocumentUsage',
                 'polynum.base.models.request.DocumentLicence',),
        ))

        self.children.append(modules.ModelList(
            _(u'Profils de reprographie'),
            draggable=True,
            collapsible=True,
            deletable=False,
            models=('polynum.base.models.profile.Profile',
                'polynum.base.models.profile.ProfileOption',
                #'polynum.base.models.profile.ProfileOptionChoice',
                ),
        ))

        self.children.append(modules.ModelList(
            _(u'Gestion du workflow'),
            draggable=True,
            collapsible=True,
            deletable=False,
            models=('polynum.base.models.workflow.Status',
                'polynum.base.models.workflow.Action',
                'polynum.base.models.workflow.Transition',
                'polynum.base.models.workflow.MailNotification',
                ),
        ))

        self.children.append(modules.ModelList(
            _(u'Entités'),
            draggable=True,
            collapsible=True,
            deletable=False,
            models=('polynum.base.models.entity.Entity',
                'polynum.base.models.entity.EntityType',),
        ))


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for git.
    """

    # we disable title because its redundant with the model list module
    title = ''
    columns = 2

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models,
                draggable=True, collapsible=True, deletable=False),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
