import pkg_resources

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.contrib.auth.decorators import login_required, user_passes_test
admin.autodiscover()

from utils import decorated_includes
import request.urls
import delegation.urls
from base.decorators import combine_decorators, user_has_validated_cgu
from base.admin import site as admin_site


request_decorator = combine_decorators(
        login_required,
        user_passes_test(user_has_validated_cgu,
            login_url=reverse_lazy('cgu-view')))

# Load plugin applications
after_urls = []
before_urls = []

for entrypoint in pkg_resources.iter_entry_points('polynum_plugin'):
    plugin_class = entrypoint.load()
    plugin = plugin_class()
    for after, urls in plugin.get_urls():
        if after:
            after_urls.append(url(r'^', include(urls)))
        else:
            before_urls.append(url(r'^', include(urls)))

urlpatterns = patterns('', *before_urls)

urlpatterns += patterns('',
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin_tools/', include('admin_tools.urls')), # django-admin-tools
    url(r'^admin/', include(admin_site.urls)),

    url(r'^request/',
        decorated_includes(request_decorator, include(request.urls.urlpatterns))),
    url(r'^remote_request/', 'polynum.request.views.remote_request'),
    url(r'^delegation/',
        decorated_includes(request_decorator, include(delegation.urls.urlpatterns))),
    url(r'^', include('polynum.pages.urls')),
    url(r'^', include('polynum.entity.urls')),
    url(r'^oai/', include('polynum.oai.urls')),
    url(r'^editor/', include('polynum.editor.urls')),
    url(r'^su/(?P<username>.*)/$', 'polynum.base.views.su', {'redirect_url': '/'}),
    url(r'^cgu/$', 'polynum.base.views.cgu', name='cgu-view'),
    url(r'^public-document/(?P<pk>\d*)/$',
        'polynum.base.views.public_document', name='public-document'),
    url(r'^public-document/(?P<pk>\d*)/(?P<name>.*)$',
        'polynum.base.views.public_document', name='public-document-with-name')
    )

if 'polynum.base.backends.CASBackend' in settings.AUTHENTICATION_BACKENDS:
    urlpatterns += patterns('',
            url(r'^accounts/login/$', 'polynum.base.cas_views.login', name='login'),
            url(r'^accounts/logout/$', 'django_cas.views.logout', name='logout'),
            )
else:
    urlpatterns += patterns('',
            url(r'^accounts/login/$', 'django.contrib.auth.views.login', 
                {'template_name': 'login.html'} if not settings.CAS_SERVER_URL else {}, name='login'),
            url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', 
                {'template_name': 'logout.html'} if not settings.CAS_SERVER_URL else {}, name='logout'),
            )

# handle static files directly -- useless in production
urlpatterns += patterns('',
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.STATIC_ROOT,
}),
)

urlpatterns += patterns('', *after_urls)
