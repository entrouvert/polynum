from django.conf import settings

OAI_ADMIN_EMAIL = getattr(settings, 'OAI_ADMIN_EMAIL', 'admin@univ.fr')

OAI_TEXT = getattr(settings, 'OAI_TEXT', 'Documents PolyNum')

OAI_REPOSITORY_NAME = getattr(settings, 'OAI_REPOSITORY_NAME',
    'PolyNum OAI-PMH repository')

OAI_PAGINATE = getattr(settings, 'OAI_PAGINATE', 10)

# champ extra pour dc
OAI_DC_EXTRA = getattr(settings, 'OAI_DC_EXTRA', {})
