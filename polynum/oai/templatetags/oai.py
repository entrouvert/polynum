# -*- encoding: utf-8 -*-

from django import template

register = template.Library()

def utcdatetime(dt):
    return dt.strftime('%Y-%m-%dT%H:%M:%SZ')

register.filter(utcdatetime)

