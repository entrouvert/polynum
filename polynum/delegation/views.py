# -*- coding: utf-8 -*-

from django.views.generic import list as list_views, edit

import models
import forms

class BaseDelegationView(object):
    model = models.Delegation
    success_url = ".."

    def get_queryset(self):
        return models.Delegation.objects.filter(user_delegating=self.request.user)

class ListDelegation(BaseDelegationView, list_views.ListView):
    pass

class NewDelegation(BaseDelegationView, edit.CreateView):
    form_class = forms.DelegationForm

    def get_form_kwargs(self):
        kwargs = super(NewDelegation, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

class DelegationDelete(BaseDelegationView, edit.DeleteView):
    pass
