from django.contrib import admin

import models

class DelegationAdmin(admin.ModelAdmin):
    list_display = ['user_delegating__display_name',
            'user_delegated__display_name']
    raw_id_fields = ['user_delegating', 'user_delegated']

    def user_delegating__display_name(self, instance):
        return instance.user_delegating.display_name()
    user_delegating__display_name.short_description = \
            models.Delegation._meta.get_field('user_delegating').verbose_name
    user_delegating__display_name.admin_order_field = 'user_delegating'

    def user_delegated__display_name(self, instance):
        return instance.user_delegated.display_name()
    user_delegated__display_name.short_description = \
            models.Delegation._meta.get_field('user_delegated').verbose_name
    user_delegated__display_name.admin_order_field = 'user_delegated'
