# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from ..base import models as base_models

class Delegation(models.Model):

    class Meta:
        verbose_name = _(u'Délégation')
        verbose_name_plural = _(u'Délégations')
        ordering = ('-creation_date',)
        unique_together = (('user_delegating', 'user_delegated'),)

    user_delegating = models.ForeignKey(base_models.User, 
            verbose_name=_(u'Utilisateur délégant'),
            related_name='delegations_given')
    user_delegated = models.ForeignKey(base_models.User, 
            verbose_name=_(u'Utilisateur délégué'),
            related_name='delegations_received')
    creation_date = models.DateTimeField(
            verbose_name=_(u'Date de création'), 
            default=datetime.datetime.utcnow,
            editable=False,
            db_index=True)

    def clean(self):
        try:
            if self.user_delegating == self.user_delegated:
                raise ValidationError(_(u'Le délégué doit être différent du délégant'))
        except base_models.User.DoesNotExist:
            pass

    def __unicode__(self):
        return _(u'Délégation de {0} à {1}').format(self.user_delegating, self.user_delegated)
