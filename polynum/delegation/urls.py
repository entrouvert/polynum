from django.conf.urls import url, patterns

from . import views

urlpatterns = patterns('',
    url(r'^$',
        views.ListDelegation.as_view(),
        name='delegation-list'),
    url(r'^new/$',
        views.NewDelegation.as_view(),
        name='new-delegation'),
    url(r'^delete-(?P<pk>\d+)/$', 
        views.DelegationDelete.as_view(),
        name='delegation-delete'),
)

