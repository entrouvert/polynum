# -*- coding: utf-8 -*-
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Request display
PAGINATE_BY = getattr(settings, "PAGINATE_BY", 50)

# Maximum size of documents, default: 50Mo
MAX_DOCUMENT_SIZE = getattr(settings, 'MAX_DOCUMENT_SIZE', 50*1024*1024)

# Minimum and maximum depth for entity filters
ENTITY_ROOTS_DEPTH = getattr(settings, 'ENTITY_ROOTS_DEPTH', 1)
MIN_ENTITY_FILTER_DEPTH = getattr(settings, 'MIN_ENTITY_FILTER_DEPTH', ENTITY_ROOTS_DEPTH)
MAX_ENTITY_FILTER_DEPTH = getattr(settings, 'MAX_DOCUMENT_SIZE', 4)

USE_PDF_VIEWER = getattr(settings, 'USE_PDF_VIEWER', False)

# entity level to show
ENTITY_TYPE_TO_SHOW = getattr(settings, 'ENTITY_TYPE_TO_SHOW',
       ((_(u'Service'), 'service'),
        (_(u'Diplôme'), 'diplome'),
        (_(u'Version diplôme'), 'version diplome'),
))
