# -*- encoding: utf-8 -*-

from django import template

from ...base import models


register = template.Library()

@register.inclusion_tag('current_requests.html', takes_context=True)
def current_requests(context):
    request = context['request']
    ctx = {}
    qs = models.Request.objects.filter(user=request.user)
    STATUS = models.Request.STATUS
    ctx['drafts'] = qs.filter(status=STATUS.brouillon.id)
    ctx['suspended'] = qs.filter(status__in=STATUS.suspended)
    ctx['waiting'] = qs.filter(status__in=STATUS.waiting)
    return ctx


