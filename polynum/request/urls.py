from django.conf.urls import url, patterns, include
from ajax_select import urls as ajax_select_urls

from . import views

urlpatterns = patterns('',
    url(r'^$', views.ListRequest.as_view(), name='list_request'),
    url(r'^csv/$', views.CSVListRequest.as_view(), name='csv_list_request'),
    url(r'^new/$', views.new_request, name='new_request'),
    url(r'^autocomplete/$', views.autocomplete, name='autocomplete'),
    url(r'^(?P<pk>\d+)/$', views.RequestDetails.as_view(), name='request_detail'),
    url(r'^(?P<pk>\d+)/history/$', views.RequestHistory.as_view(), name='request_history'),
    url(r'^(?P<pk>\d+)/action/(?P<workflow_pk>.*)/$', views.request_action, name='request_action'),
    url(r'^(?P<pk>\d+)/delete/$', views.request_delete, name='request_delete'),
    url(r'^(?P<pk>\d+)/duplicate/$', views.request_duplicate, name='request_duplicate'),
    url(r'^(?P<pk>\d+)/download/.*$', views.request_download, name='request_download'),
    url(r'^(?P<pk>\d+)/edit/(?P<step>.+)/$', views.request_wizard_step, name='request_wizard_step'),
    url(r'^(?P<pk>\d+)/edit/$', views.request_wizard_step, name='request_wizard'),
    url(r'^finish_remote_request/(?P<pk>\d+)/$', views.finish_remote_request, name='finish_remote_request'),
    url(r'^lookups/', include(ajax_select_urls)),
)

