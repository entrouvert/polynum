from django.db import models

class RemoteRequest(models.Model):
    '''Holds a file for the printing Web-Service'''
    uploadfile = models.FileField(upload_to='remote_request', blank=True)
    entity = models.ForeignKey('base.Entity',
            on_delete=models.PROTECT, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
