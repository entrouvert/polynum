# -*- coding: utf-8 -*-
# https://github.com/mfenniak/pyPdf
from collections import defaultdict
import logging

logger = logging.getLogger(__name__)

from pyPdf import PdfFileReader
from pyPdf.utils import PyPdfError

import magic

from ..base.models import Request
from ..utils import cache_to_django

N_ = lambda x: x

def check_pdf(filelike):
    try:
        try:
            pdf_file = PdfFileReader(filelike)
        except:
            logger.exception('Invalid pdf file')
            raise ValueError(N_(u'Ce fichier PDF est invalide'))
        if pdf_file.isEncrypted and pdf_file.decrypt('') != 1:
            raise ValueError(N_(u'Les fichiers PDF chiffrés ne sont pas acceptés.'))
        pdf_file.documentInfo
    except PyPdfError:
        raise ValueError(N_(u"Vous ne pouvez envoyer que des fichiers PDF."))

def fill_document_attributes_from_pdf_file(document, docfile):
    if not docfile:
        return
    try:
        docfile.open()
        mime_type = magic.from_buffer(docfile.read(), mime=True)
        docfile.seek(0)
        if mime_type == 'application/pdf':
            try:
                pdf_file = PdfFileReader(docfile)
            except:
                logger.exception('Invalid pdf file')
                raise ValueError(N_(u'Ce fichier PDF est invalide'))
            if pdf_file.isEncrypted and pdf_file.decrypt('') != 1:
                raise ValueError(N_(u'Les fichiers PDF chiffrés ne sont pas acceptés.'))
            pdf_info = pdf_file.documentInfo
            if pdf_info is None:
                return
            titles = filter(None, [pdf_info.title, pdf_info.subject])
            document.name = u' - '.join(titles)[:60]
            document.nb_pages = pdf_file.numPages or 0
            return
        if mime_type == 'image/jpeg':
            return
        raise ValueError(N_(u"Ce type de fichier n'est pas accepté"))
    except PyPdfError:
        raise ValueError(N_(u"Vous ne pouvez envoyer que des fichiers PDF."))

@cache_to_django(timeout=300)
def field_completion(user, field_names, without_scores=True):
    qs = Request.objects.filter(user=user)
    scores = defaultdict(lambda: defaultdict(lambda: 0))
    for values in qs.values_list(*field_names):
        for field_name, value in zip(field_names, values):
            if value is not None:
                scores[field_name][value] += 1
    for key, value in scores.iteritems():
        scores[key] = sorted(scores[key].items(), key=lambda x: x[1])
        if without_scores:
            scores[key] = map(lambda x: x[0], scores[key])
        scores[key] = scores[key][:3] + sorted(scores[key][3:])
    return scores

