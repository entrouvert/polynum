var PolynumDelegate = function(target) {
  var $target = $('#' + target)
  return {
    target: $target,
    entity_type: $target.data('entity-type'),
    cache: {},
    selected: undefined,
    _getJSON: function(url) {
      var res;
      $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: function(data) {
              res = data;
            },
            async: false
      });
      return res;
    },
    getParents: function(item) {
      var url = '/entity/' + item + '/parents/';
      var data = this._getJSON(url);
      return data.slice(1)
    },
    _getItem: function(item, forchildren) {
      var url = item ? '/entity/' + item + '/' : '/entity/';
      var res = null;
      if (! (item in this.cache) || 
          (forchildren && ! ('children' in this.cache[item]))) {
        res = this._getJSON(url);
        for (var i = 0; i < res.children.length; i++) {
          var child = res.children[i];
          this.cache[child.id] = $.extend(this.cache[child.id] || {}, child);
        }
        this.cache[item] = $.extend(this.cache[item] || {}, res);
      }
      return this.cache[item];
    },
    getRootItem: function () {
       return null;
    },
    numberOfChildrenOfItem: function(item) {
       var data = this._getItem(item, true);
       return data ? data.children.length : 0;
    },
    childOfItem: function(index, item) {
       var data = this._getItem(item, true);
       return data ? data.children[index].id : null;
    },
    objectValueForItem: function(item, $row) {
       var data = this._getItem(item);
       if (data) {
         res = $('<span/>')
           .text(data.name);
         $($row)
           .addClass(data.type.replace(' ', '-'))
           .data('title', data.code)
           .data('content', data.description + '<br/>' + data.type)
           .popover({placement: 'top', delay: {show: 100, hide: 100},
             trigger: 'hover'});
         if (this.entity_type && data.type == this.entity_type) {
           $($row).addClass('highlight');
         }
         return res;
       } else {
         return 'null';
       }
    },
    isLeafItem: function(item) {
       var data = this._getItem(item);
       return data ? data.is_leaf : false;
    },
    selectItem: function(item) {
       if (this.entity_type) {
         var data = this._getItem(item, false);
         if (data.type != this.entity_type) {
           item = '';
         }
       }
       if (this.target.val() != item) {
         this.target.val(item);
         this.target.trigger('change');
       }
       this.selected = item;
    },
    init: function(widget) {
       var val = this.target.val();
       this.target.on('change', $.proxy(this.change, this));
       if (val) {
         widget.setItem(val);
       }
    }
  }
};
(function($) {
  var done = 0;
  $(function () {
    if (! done) {
      done = 1;
      window.install_django_miller_columns = function () {
        $('.django-miller-columns-widget').each(function (i, e) {
          var delegate = new PolynumDelegate($(e).data('target'));
          var f = e;
          $(e).millercolumns({delegate: delegate });
          delegate.target.data('update', function (val) {
            $(f).millercolumns('setItem', val);
          });
        });
      }
      window.install_django_miller_columns();
    }
//      get_children: function (row) {
//        var data = $(row).data('rowData');
//        var id = data.id;
//        var res = [];
//        $.ajax({
//              type: 'GET',
//              url: '/entity/' + id + '/',
//              dataType: 'json',
//              success: function(data) {
//                for (i = 0; i < data.children.length; i++) {
//                  var child = data.children[i];
//                  res.push({
//                    label: child.name,
//                    data: { id: child.id },
//                  });
//                }
//              },
//              async: false
//        });
//        return res;
//      },
//      select: function (ev) {
//         alert(this);
//      },
//    });
 });
})(jQuery)
