# -*- coding: utf-8 -*-
"""
Gestion des rôles et autorisations :
- un groupe a des rôles sur certaines entités
- un rôles peut faire certaines actions sur les demandes selon leur status
"""

from django.db import models
from django.utils.translation import ugettext_lazy as _

import user

class Role(models.Model):

    class Meta:
        app_label = 'base'
        verbose_name = _(u'Rôle')
        verbose_name_plural = _(u'Rôles')

    def __unicode__(self):
        return self.name

    id = models.CharField(max_length=40, primary_key=True, verbose_name=_(u'Code rôle'))
    name = models.CharField(max_length=128, verbose_name=_(u'Nom du rôle'))


class RoleAssociation(models.Model):

    class Meta:
        app_label = 'base'
        verbose_name = _(u"Association utilisateur,entité -> rôle")
        verbose_name_plural = _(u"Associations utilisateur,entité -> rôle")

    def __unicode__(self):
        return u'%s,%s -> %s' % (self.user, self.entity, self.role)

    user = models.ForeignKey(user.User, verbose_name=_(u'Utilisateur'))
    entity = models.ForeignKey('Entity', verbose_name=_(u'Entité administrative'))
    role = models.ForeignKey('Role', verbose_name=_(u'Rôle'))
