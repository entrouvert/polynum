# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

from decimal import Decimal
Zero = Decimal('0.00')

from model_utils.managers import QueryManager

class ProfileOptionManager(models.Manager):
    def get_query_set(self):
        return super(ProfileOptionManager, self).get_query_set().prefetch_related('profileoptionchoice_set')

class ProfileVisibleOptionManager(QueryManager, ProfileOptionManager):
    use_for_related_fields = False

class ProfileOption(models.Model):
    objects = ProfileOptionManager()
    visibles = ProfileVisibleOptionManager(visible=True)

    class Meta:
        app_label = 'base'
        verbose_name = _(u"Profils d'impression : Option")
        verbose_name_plural = _(u"Profils d'impression : Options")
        ordering = ('order', 'name')

    def __unicode__(self):
        return self.name

    TYPE_CHOICES = (
            ('R', 'Radio (choix unique)'),
            ('L', 'Liste (choix unique)'),
            ('C', 'Cases à cocher (choix multiple)'),
            ('LM', 'Liste (choix multiple)'),
    )

    name = models.CharField(max_length=128, verbose_name=_(u"Nom de l'option de profil d'impression"))
    visible = models.BooleanField(verbose_name=_(u'Visible'), default=True, blank=True)
    list_type = models.CharField(max_length=2, choices=TYPE_CHOICES, verbose_name=_(u'Type de liste de choix'))
    description = models.TextField(verbose_name=_(u'Description'), blank=True)
    order = models.IntegerField(verbose_name=_(u'Ordre'), default=0)

    def display_choices(self):
        return u', '.join([c.name for c in self.profileoptionchoice_set.all()])
    display_choices.short_description = 'Choix possibles'

class ProfileOptionChoiceManager(models.Manager):
    def get_query_set(self):
        return super(ProfileOptionChoiceManager, self).get_query_set().select_related('option')

class ProfileOptionChoice(models.Model):
    objects = models.Manager() # default manager
    visibles = QueryManager(visible=True)

    class Meta:
        app_label = 'base'
        verbose_name = _(u"Profils d'impression : Choix")
        verbose_name_plural = _(u"Profils d'impression : Choix")
        ordering = ('order', 'name')

    def __unicode__(self):
        return u'%s: %s' % (self.option.name, self.name)

    name = models.CharField(max_length=128, verbose_name=_(u'Nom du choix'))
    visible = models.BooleanField(verbose_name=_(u'Visible'), default=True, blank=True)
    option = models.ForeignKey(ProfileOption, verbose_name=_(u'Option de profil à laquelle ce choix est lié'))
    ppp = models.DecimalField(max_digits=10, decimal_places=5,
            verbose_name=_(u'Prix par page (en euros)'), default=Zero)
    ppd = models.DecimalField(max_digits=10, decimal_places=5,
            verbose_name=_(u'Prix par document (en euros)'), default=Zero)
    description = models.TextField(verbose_name=_(u'Description'), blank=True)
    order = models.IntegerField(verbose_name=_(u'Ordre'), default=0)

class ProfileManager(models.Manager):
    def get_query_set(self):
        return super(ProfileManager, self).get_query_set().prefetch_related('choices__option')

class Profile(models.Model):
    objects = ProfileManager()
    visibles = QueryManager(visible=True)

    class Meta:
        app_label = 'base'
        verbose_name = _(u"Profil d'impression")
        verbose_name_plural = _(u"Profils d'impression")
        ordering = ('order', 'name')

    def __unicode__(self):
        return self.name

    name = models.CharField(max_length=128, verbose_name=_(u"Nom du profil d'impression"))
    visible = models.BooleanField(verbose_name=_(u'Visible'), default=True, blank=True)
    choices = models.ManyToManyField(ProfileOptionChoice, verbose_name=_(u'Choix associés au profil'), blank=True)
    ppp = models.DecimalField(max_digits=10, decimal_places=5,
            verbose_name=_(u'Prix par page (en euros)'), default=Zero)
    ppd = models.DecimalField(max_digits=10, decimal_places=5,
            verbose_name=_(u'Prix par document (en euros)'), default=Zero)
    description = models.TextField(verbose_name=_(u'Description'), blank=True)
    order = models.IntegerField(verbose_name=_(u'Ordre'), default=0)

    def display_choices(self):
        choices = sorted([c for c in self.choices.all() if c.option.visible == True], key=lambda k: k.option.order)
        li = u''.join([u'<li>%s</li>' % c for c in choices])
        return '<ul>%s</ul>' % li
    display_choices.short_description = 'Choix associés au profil'
    display_choices.allow_tags = True


