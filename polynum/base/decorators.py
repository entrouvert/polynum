from django.core.exceptions import ObjectDoesNotExist

def combine_decorators(*decorators):
    def f(function):
        for decorator in reversed(decorators):
            function = decorator(function)
        return function
    return f

def user_has_validated_cgu(user):
    try:
        profile = user.get_profile()
        return profile.accepted_cgu
    except ObjectDoesNotExist:
        return False
