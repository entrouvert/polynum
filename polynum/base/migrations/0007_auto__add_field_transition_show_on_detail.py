# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Transition.show_on_detail'
        db.add_column('base_transition', 'show_on_detail',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

    def backwards(self, orm):
        # Deleting field 'Transition.show_on_detail'
        db.delete_column('base_transition', 'show_on_detail')

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'base.accountingcode': {
            'Meta': {'ordering': "('entity', 'code')", 'object_name': 'AccountingCode'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Entity']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'base.action': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Action'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'comment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'edit_pages_csv': ('polynum.base.fields.MultiSelectField', [], {'max_length': '1024', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'special_type': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'ui_message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'validate_request': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'base.deliveryplace': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'DeliveryPlace'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'base.documentlicence': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'DocumentLicence'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'base.documentusage': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'DocumentUsage'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'base.entity': {
            'Meta': {'ordering': "('left_bound',)", 'object_name': 'Entity'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'depth': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'db_index': 'True', 'blank': 'True'}),
            'description_override': ('django.db.models.fields.TextField', [], {'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'left_bound': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '128', 'blank': 'True'}),
            'name_override': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '128', 'blank': 'True'}),
            'right_bound': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'types': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['base.EntityType']", 'null': 'True', 'blank': 'True'})
        },
        'base.entitytype': {
            'Meta': {'object_name': 'EntityType'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'base.history': {
            'Meta': {'ordering': "('date',)", 'object_name': 'History'},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Action']", 'on_delete': 'models.PROTECT'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'new_status': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'on_delete': 'models.PROTECT', 'to': "orm['base.Status']"}),
            'old_status': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'on_delete': 'models.PROTECT', 'to': "orm['base.Status']"}),
            'request': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Request']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'})
        },
        'base.mailnotification': {
            'Meta': {'object_name': 'MailNotification'},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Action']"}),
            'body_template': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject_template': ('django.db.models.fields.TextField', [], {}),
            'to': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        'base.profile': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'Profile'},
            'choices': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['base.ProfileOptionChoice']", 'symmetrical': 'False', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'ppd': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '7', 'decimal_places': '3'}),
            'ppp': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '7', 'decimal_places': '3'})
        },
        'base.profileoption': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'ProfileOption'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'base.profileoptionchoice': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'ProfileOptionChoice'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'option': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.ProfileOption']"}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'ppd': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '7', 'decimal_places': '3'}),
            'ppp': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '7', 'decimal_places': '3'})
        },
        'base.request': {
            'Meta': {'ordering': "('-creation_date', '-month_order')", 'object_name': 'Request'},
            'base_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Profile']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'choices': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['base.ProfileOptionChoice']", 'symmetrical': 'False', 'blank': 'True'}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'contact_bureau': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'contact_telephone1': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'contact_telephone2': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'copies': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'copyright': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'cost': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '7', 'decimal_places': '2', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.utcnow', 'db_index': 'True'}),
            'delivery_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2012, 7, 26, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'delivery_place': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.DeliveryPlace']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'details': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Entity']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'financial_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '64', 'blank': 'True'}),
            'financial_comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licence': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.DocumentLicence']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'modification_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'month_order': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'nb_pages': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'sponsor': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Status']", 'on_delete': 'models.PROTECT'}),
            'uploadfile': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'usage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.DocumentUsage']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'base.role': {
            'Meta': {'object_name': 'Role'},
            'id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'base.roleassociation': {
            'Meta': {'object_name': 'RoleAssociation'},
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Entity']", 'on_delete': 'models.DO_NOTHING'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Role']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'base.status': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'Status'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'end': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'start': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'visible_by': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['base.Role']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'base.transition': {
            'Meta': {'ordering': "('role', 'source')", 'unique_together': "(('role', 'source', 'action'),)", 'object_name': 'Transition'},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Action']"}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'destination': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'in_transitions'", 'to': "orm['base.Status']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_override': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Role']"}),
            'show_on_detail': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'out_transitions'", 'to': "orm['base.Status']"}),
            'warn': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['base']