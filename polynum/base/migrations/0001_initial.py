# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ProfileOption'
        db.create_table('base_profileoption', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('visible', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('list_type', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('base', ['ProfileOption'])

        # Adding model 'ProfileOptionChoice'
        db.create_table('base_profileoptionchoice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('option', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.ProfileOption'])),
            ('ppp', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=7, decimal_places=3)),
            ('ppd', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=7, decimal_places=3)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('base', ['ProfileOptionChoice'])

        # Adding model 'Profile'
        db.create_table('base_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('ppp', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=7, decimal_places=3)),
            ('ppd', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=7, decimal_places=3)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('base', ['Profile'])

        # Adding M2M table for field choices on 'Profile'
        db.create_table('base_profile_choices', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('profile', models.ForeignKey(orm['base.profile'], null=False)),
            ('profileoptionchoice', models.ForeignKey(orm['base.profileoptionchoice'], null=False))
        ))
        db.create_unique('base_profile_choices', ['profile_id', 'profileoptionchoice_id'])

        # Adding model 'Role'
        db.create_table('base_role', (
            ('id', self.gf('django.db.models.fields.CharField')(max_length=40, primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal('base', ['Role'])

        # Adding model 'RoleAssociation'
        db.create_table('base_roleassociation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Group'])),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Entity'], on_delete=models.DO_NOTHING)),
            ('role', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Role'])),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('base', ['RoleAssociation'])

        # Adding model 'Status'
        db.create_table('base_status', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40)),
            ('visible', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('default', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('start', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('end', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('base', ['Status'])

        # Adding M2M table for field visible_by on 'Status'
        db.create_table('base_status_visible_by', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('status', models.ForeignKey(orm['base.status'], null=False)),
            ('role', models.ForeignKey(orm['base.role'], null=False))
        ))
        db.create_unique('base_status_visible_by', ['status_id', 'role_id'])

        # Adding model 'Action'
        db.create_table('base_action', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('ui_message', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('comment', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('special_type', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('edit_pages_csv', self.gf('polynum.base.fields.MultiSelectField')(max_length=1024, blank=True)),
            ('validate_request', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('base', ['Action'])

        # Adding model 'MailNotification'
        db.create_table('base_mailnotification', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('action', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Action'])),
            ('to', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('subject_template', self.gf('django.db.models.fields.TextField')()),
            ('body_template', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('base', ['MailNotification'])

        # Adding model 'Transition'
        db.create_table('base_transition', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('role', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Role'])),
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(related_name='out_transitions', to=orm['base.Status'])),
            ('destination', self.gf('django.db.models.fields.related.ForeignKey')(related_name='in_transitions', to=orm['base.Status'])),
            ('action', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Action'])),
            ('default', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('warn', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('base', ['Transition'])

        # Adding unique constraint on 'Transition', fields ['role', 'source', 'action']
        db.create_unique('base_transition', ['role_id', 'source_id', 'action_id'])

        # Adding model 'DocumentUsage'
        db.create_table('base_documentusage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('base', ['DocumentUsage'])

        # Adding model 'DocumentLicence'
        db.create_table('base_documentlicence', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('base', ['DocumentLicence'])

        # Adding model 'DeliveryPlace'
        db.create_table('base_deliveryplace', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('base', ['DeliveryPlace'])

        # Adding model 'Request'
        db.create_table('base_request', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('month_order', self.gf('django.db.models.fields.IntegerField')(default=-1)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('uploadfile', self.gf('django.db.models.fields.files.FileField')(max_length=100, blank=True)),
            ('nb_pages', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('usage', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.DocumentUsage'], null=True, on_delete=models.PROTECT, blank=True)),
            ('licence', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.DocumentLicence'], null=True, on_delete=models.PROTECT, blank=True)),
            ('copyright', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('sponsor', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Entity'], null=True, on_delete=models.PROTECT, blank=True)),
            ('copies', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('status', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Status'], on_delete=models.PROTECT)),
            ('base_profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Profile'], null=True, on_delete=models.PROTECT, blank=True)),
            ('details', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('delivery_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2012, 7, 20, 0, 0), null=True, blank=True)),
            ('delivery_place', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.DeliveryPlace'], null=True, on_delete=models.PROTECT, blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('creation_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.utcnow, db_index=True)),
            ('modification_date', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('contact_email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('contact_telephone1', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('contact_telephone2', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('contact_bureau', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('financial_code', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=64, blank=True)),
            ('financial_comment', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('cost', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=7, decimal_places=2, blank=True)),
        ))
        db.send_create_signal('base', ['Request'])

        # Adding M2M table for field choices on 'Request'
        db.create_table('base_request_choices', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('request', models.ForeignKey(orm['base.request'], null=False)),
            ('profileoptionchoice', models.ForeignKey(orm['base.profileoptionchoice'], null=False))
        ))
        db.create_unique('base_request_choices', ['request_id', 'profileoptionchoice_id'])

        # Adding model 'History'
        db.create_table('base_history', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, db_index=True, blank=True)),
            ('request', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Request'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, on_delete=models.SET_NULL, blank=True)),
            ('action', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Action'], on_delete=models.PROTECT)),
            ('old_status', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, on_delete=models.PROTECT, to=orm['base.Status'])),
            ('new_status', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', on_delete=models.PROTECT, to=orm['base.Status'])),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('base', ['History'])

        # Adding model 'EntityType'
        db.create_table('base_entitytype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('base', ['EntityType'])

        # Adding model 'Entity'
        db.create_table('base_entity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=64, db_index=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=128, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(db_index=True, blank=True)),
            ('name_override', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=128, blank=True)),
            ('description_override', self.gf('django.db.models.fields.TextField')(db_index=True, blank=True)),
            ('depth', self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True)),
            ('left_bound', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True, db_index=True)),
            ('right_bound', self.gf('django.db.models.fields.PositiveIntegerField')(unique=True, db_index=True)),
        ))
        db.send_create_signal('base', ['Entity'])

        # Adding M2M table for field types on 'Entity'
        db.create_table('base_entity_types', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('entity', models.ForeignKey(orm['base.entity'], null=False)),
            ('entitytype', models.ForeignKey(orm['base.entitytype'], null=False))
        ))
        db.create_unique('base_entity_types', ['entity_id', 'entitytype_id'])

        # Adding model 'AccountingCode'
        db.create_table('base_accountingcode', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Entity'])),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=32, db_index=True)),
        ))
        db.send_create_signal('base', ['AccountingCode'])

    def backwards(self, orm):
        # Removing unique constraint on 'Transition', fields ['role', 'source', 'action']
        db.delete_unique('base_transition', ['role_id', 'source_id', 'action_id'])

        # Deleting model 'ProfileOption'
        db.delete_table('base_profileoption')

        # Deleting model 'ProfileOptionChoice'
        db.delete_table('base_profileoptionchoice')

        # Deleting model 'Profile'
        db.delete_table('base_profile')

        # Removing M2M table for field choices on 'Profile'
        db.delete_table('base_profile_choices')

        # Deleting model 'Role'
        db.delete_table('base_role')

        # Deleting model 'RoleAssociation'
        db.delete_table('base_roleassociation')

        # Deleting model 'Status'
        db.delete_table('base_status')

        # Removing M2M table for field visible_by on 'Status'
        db.delete_table('base_status_visible_by')

        # Deleting model 'Action'
        db.delete_table('base_action')

        # Deleting model 'MailNotification'
        db.delete_table('base_mailnotification')

        # Deleting model 'Transition'
        db.delete_table('base_transition')

        # Deleting model 'DocumentUsage'
        db.delete_table('base_documentusage')

        # Deleting model 'DocumentLicence'
        db.delete_table('base_documentlicence')

        # Deleting model 'DeliveryPlace'
        db.delete_table('base_deliveryplace')

        # Deleting model 'Request'
        db.delete_table('base_request')

        # Removing M2M table for field choices on 'Request'
        db.delete_table('base_request_choices')

        # Deleting model 'History'
        db.delete_table('base_history')

        # Deleting model 'EntityType'
        db.delete_table('base_entitytype')

        # Deleting model 'Entity'
        db.delete_table('base_entity')

        # Removing M2M table for field types on 'Entity'
        db.delete_table('base_entity_types')

        # Deleting model 'AccountingCode'
        db.delete_table('base_accountingcode')

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'base.accountingcode': {
            'Meta': {'ordering': "('entity', 'code')", 'object_name': 'AccountingCode'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Entity']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'base.action': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Action'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'comment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'edit_pages_csv': ('polynum.base.fields.MultiSelectField', [], {'max_length': '1024', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'special_type': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'ui_message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'validate_request': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'base.deliveryplace': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'DeliveryPlace'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'base.documentlicence': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'DocumentLicence'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'base.documentusage': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'DocumentUsage'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'base.entity': {
            'Meta': {'ordering': "('left_bound',)", 'object_name': 'Entity'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'depth': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'db_index': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'db_index': 'True', 'blank': 'True'}),
            'description_override': ('django.db.models.fields.TextField', [], {'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'left_bound': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '128', 'blank': 'True'}),
            'name_override': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '128', 'blank': 'True'}),
            'right_bound': ('django.db.models.fields.PositiveIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'types': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['base.EntityType']", 'null': 'True', 'blank': 'True'})
        },
        'base.entitytype': {
            'Meta': {'object_name': 'EntityType'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'base.history': {
            'Meta': {'ordering': "('date',)", 'object_name': 'History'},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Action']", 'on_delete': 'models.PROTECT'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'new_status': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'on_delete': 'models.PROTECT', 'to': "orm['base.Status']"}),
            'old_status': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'on_delete': 'models.PROTECT', 'to': "orm['base.Status']"}),
            'request': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Request']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'})
        },
        'base.mailnotification': {
            'Meta': {'object_name': 'MailNotification'},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Action']"}),
            'body_template': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject_template': ('django.db.models.fields.TextField', [], {}),
            'to': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        'base.profile': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'Profile'},
            'choices': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['base.ProfileOptionChoice']", 'symmetrical': 'False', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'ppd': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '7', 'decimal_places': '3'}),
            'ppp': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '7', 'decimal_places': '3'})
        },
        'base.profileoption': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'ProfileOption'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'base.profileoptionchoice': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'ProfileOptionChoice'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'option': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.ProfileOption']"}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'ppd': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '7', 'decimal_places': '3'}),
            'ppp': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '7', 'decimal_places': '3'})
        },
        'base.request': {
            'Meta': {'ordering': "('-creation_date', '-month_order')", 'object_name': 'Request'},
            'base_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Profile']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'choices': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['base.ProfileOptionChoice']", 'symmetrical': 'False', 'blank': 'True'}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'contact_bureau': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'contact_telephone1': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'contact_telephone2': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'copies': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'copyright': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'cost': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '7', 'decimal_places': '2', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.utcnow', 'db_index': 'True'}),
            'delivery_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2012, 7, 20, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'delivery_place': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.DeliveryPlace']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'details': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Entity']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'financial_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '64', 'blank': 'True'}),
            'financial_comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licence': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.DocumentLicence']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'modification_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'month_order': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'nb_pages': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'sponsor': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Status']", 'on_delete': 'models.PROTECT'}),
            'uploadfile': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'usage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.DocumentUsage']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'base.role': {
            'Meta': {'object_name': 'Role'},
            'id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'base.roleassociation': {
            'Meta': {'object_name': 'RoleAssociation'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Entity']", 'on_delete': 'models.DO_NOTHING'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Role']"})
        },
        'base.status': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'Status'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'end': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'start': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'visible_by': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['base.Role']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'base.transition': {
            'Meta': {'ordering': "('role', 'source')", 'unique_together': "(('role', 'source', 'action'),)", 'object_name': 'Transition'},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Action']"}),
            'default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'destination': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'in_transitions'", 'to': "orm['base.Status']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Role']"}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'out_transitions'", 'to': "orm['base.Status']"}),
            'warn': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['base']