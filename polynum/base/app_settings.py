from django.conf import settings

# Polynum CAS Backend configuration
LDAP_URL = getattr(settings, 'LDAP_URL', '')
LDAP_BIND_DN = getattr(settings, 'LDAP_BIND_DN', '')
LDAP_BIND_PASSWORD = getattr(settings, 'LDAP_BIND_PASSWORD', '')
LDAP_BASE = getattr(settings, 'LDAP_BASE', '')
LDAP_USER_QUERY = getattr(settings, 'LDAP_USER_QUERY', '')

# Request minimum delivery delay, measured in days
MINIMUM_DELIVERY_DELAY = getattr(settings, 'MINIMUM_DELIVERY_DELAY', 2)

# Maximum lifetime for request, measured in days
MAXIMUM_DAYS_BEFORE_PURGE = 545

# LDAP Completion
LDAP_COMPLETION_QUERY_FILTER = getattr(settings,
    'LDAP_COMPLETION_QUERY_FILTER', '(|(supannAliasLogin=*{0}*)(displayName=*{0}*))')
LDAP_COMPLETION_QUERY_ATTRIBUTES = getattr(settings, 
        'LDAP_COMPLETION_QUERY_ATTRIBUTES',
        [ 'displayName', 'givenName', 'sn', 'updLogin', 'supannAliasLogin' ])
LDAP_COMPLETION_QUERY_TEMPLATE = getattr(settings,
    'LDAP_COMPLETION_QUERY_TEMPLATE', u'{displayName} ({updLogin})')
