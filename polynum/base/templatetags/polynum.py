# -*- encoding: utf-8 -*-

from django import template
from django.utils.safestring import mark_safe
from django.utils.html import escape

register = template.Library()

def widont(value):
    bits = escape(value).rsplit(' ', 1)
    try:
        widowless = u'&nbsp;'.join(bits)
        return mark_safe(widowless)
    except:
        return value

register.filter(widont)

