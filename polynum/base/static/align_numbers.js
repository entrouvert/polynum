/* Copied from http://stackoverflow.com/questions/1363239/aligning-decimal-points-in-html */

/* This function will align table columns on the char if in the col from the 
 * colgroup has the property 'align="char"' and a attribute 'char'. The alignment
 * is done on the first occurence of the specified char.
 * 
 * The function is inspired from:
 * 
 * http://krijnhoetmer.nl/stuff/javascript/table-align-char/
 * http://stackoverflow.com/questions/1363239/aligning-decimal-points-in-html
 */
function alignNumbers()
{
  var table; /* This will store the table currently working on . */
  var i = 0; /* Every column can have it's own width, the counter makes the class name unique. */

  /* Get all tables for which the alignment fix must be done. 
   *
   * Note: this could even be further optimized by just looking for tables where
   * there is a a col with 'align="char"'. 
   */
  $('table.fix-align-char').each(function(index)
  {
    table = $(this);

    /* All table columns are fetched to have a correct index, without it it's
     * hard to get the correct table cells.
     */
    $(this).find('col').each(function(index)
    {
      /* Only those table cells are changed for which the alignment is set to
       * char and a char is given.
       */
      if ($(this).prop('align') == 'char' && !empty($(this).attr('char')))
      {
        /* Variables for storing the width for the left and right part (in pixels). */
        var left_width = 0, right_width = 0;
        var col, left_part, right_part, parts, new_html;
        i++; /* Increase the counter since we are working on a new column. */
        col = $(this);

        /* For the col index + 1 (nth-child starts counting at 1), find the table
         * cells in the current table.
         */
        table.find('> tbody > tr > td:nth-child('+ (index + 1) +')').each(function(index)
        {
          /* Split the html on the specified char. */
          parts = $(this).html().split(col.attr('char'));
          new_html = '';


          /* The first element is always the left part. The remaining part(s) are
           * the right part. Should there be more chars in the string, the right
           * parts are rejoined again with the specified char. 
           */
          left_part = parts.shift();
          right_part = parts.join(',');

          /* Add a left part to the new html if the left part isn't empty*/
          if (!empty(left_part))
          {
            new_html = new_html + '<span class="left">' + left_part + '</span>';
          }

          /* Add the specified char and the right part to the new html if 
           * the right part isn't empty*/
          if (!empty(right_part))
          {
            new_html = new_html + col.attr('char') + '<span class="right">' + right_part + '</span>';
          }

          /* If there is a new html, the width must be determined and a class is
           * added.
           * 
           * Note: outerWidth is used instead of width so padding, margin and
           * borders are taken into account.
           */
          if (!empty(new_html))
          {
            $(this).html(new_html); /* Set the new html. */
            $(this).addClass('char-align-' + i); /* Add a class to the table cell. */

            /* Get the left span to determine its outer width. */
            leftSpan = $(this).children('.left');

            if (!empty(leftSpan) && left_width < leftSpan.outerWidth())
            {
              left_width = leftSpan.outerWidth();
            }

            /* Get the right span to determine its outer width. */
            rightSpan = $(this).children('.right');

            if (!empty(rightSpan) && right_width < rightSpan.outerWidth())
            {
              right_width = rightSpan.outerWidth();
            }

          }

        });

        /* Only if any width is larger then 0, add a style. */
        if (left_width > 0 || right_width > 0)
        {
          style_text = '<style type="text/css">.fix-align-char td.char-align-' + (i) + ' span.left { float: left; text-align: right; width: ' + (left_width) + 'px; }\n.fix-align-char td.char-align-' + (i) + ' span.right { float: right; text-align: left; width: ' + right_width + 'px; }</style>';
          $('head').append(style_text);
        }

      }
    });
  });

}

$(document).ready(function(){
  alignNumbers();
});
