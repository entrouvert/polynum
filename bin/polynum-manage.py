#!/usr/bin/env python2.6
import os
import sys
import site

POLYNUM_DIR = os.environ.get('POLYNUM_DIR', '/opt/polynum')
VIRTUAL_ENV = os.environ.get('VIRTUAL_ENV', os.path.join(POLYNUM_DIR, 'virtualenv'))

if not os.path.isdir(POLYNUM_DIR):
    print >> sys.stderr, POLYNUM_DIR + ' does not exist'
    sys.exit(1)

sys.path.append(POLYNUM_DIR)
sys.path.append('/etc/polynum')  # for "import local_settings"

if os.path.isdir(VIRTUAL_ENV):
    site_packages = os.path.join(VIRTUAL_ENV,
            'lib',
            'python%d.%d' % sys.version_info[:2],
            'site-packages')
    site.addsitedir(site_packages)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "polynum.settings")
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)

