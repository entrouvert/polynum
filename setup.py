#! /usr/bin/env python

''' Setup script for Polynum
'''

from setuptools import setup, find_packages

setup(name="Polynum",
      version=0.2,
      license="AGPLv3 or later",
      description="Polycopies numeriques",
      url="http://dev.entrouvert.org/projects/polynum/",
      author="Entr'ouvert",
      author_email="info@entrouvert.org",
      maintainer="Jerome Schneider",
      maintainer_email="jschneider@entrouvert.com",
      packages=find_packages(),
      package_data={},
      install_requires=[
          'django>=1.4,<1.6',
          'south',
          'django-crispy-forms',
          'django-sekizai',
          'pypdf',
          'django-cas >= 2.1.1',
          'django-admin-tools',
          'flup',
          'psycopg2',
          'django-model-utils < 1.2.0',
          'django-ajax-selects < 1.6.0',
          'raven',
          'python-entrouvert<2.0',
      ],
      dependency_links = [
          'http://repos.entrouvert.org/python-entrouvert.git/snapshot/python-entrouvert-master.zip#egg=python-entrouvert-1.999999',
      ],
)
